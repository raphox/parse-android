package com.vivace.parse_application.parseapplication.fragment;


import android.app.ListActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.vivace.parse_application.parseapplication.R;
import com.vivace.parse_application.parseapplication.model.GameScore;
import com.vivace.parse_application.parseapplication.util.Util;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {

    private static final String TAG = "ListFragment";

    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ListView listView = view.findViewById(R.id.list);

        ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>(getActivity(),
                new ParseQueryAdapter.QueryFactory<ParseObject>() {
                    @Override
                    public ParseQuery<ParseObject> create() {
                        ParseQuery parseQuery = ParseQuery.getQuery("GameScore")
                                .orderByAscending("playerName");

                        // If network is down, load items from local datastore
                        if (!Util.isNetworkAvailable(getActivity())) {
                            parseQuery.fromLocalDatastore();
                        }

                        return parseQuery;
                    }
                }, android.R.layout.simple_list_item_1);

        adapter.setTextKey("playerName");

        adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
            @Override
            public void onLoading() {
                Log.d(TAG, "loading");
            }

            @Override
            public void onLoaded(List<ParseObject> objects, Exception e) {
                Log.d(TAG, "loaded");
                if (e != null
                        && e instanceof ParseException
                        && ((ParseException) e).getCode() != ParseException.CACHE_MISS) {
                    Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                try {
                    // Storing in the local datastore to get itens when device is offline
                    GameScore.pinAll(objects);
                } catch (ParseException pe) {
                    Log.e(TAG, "Exception while storing gamescores object to db :" + pe, pe);
                }
            }
        });
        listView.setAdapter(adapter);

        return view;
    }

}
