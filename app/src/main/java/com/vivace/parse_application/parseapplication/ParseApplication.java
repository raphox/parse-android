package com.vivace.parse_application.parseapplication;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;
import com.vivace.parse_application.parseapplication.model.GameScore;

public class ParseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(GameScore.class);
        Parse.initialize(this);

        Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
    }
}
