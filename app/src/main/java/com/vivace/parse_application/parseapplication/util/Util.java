package com.vivace.parse_application.parseapplication.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by raphael on 24/01/18.
 */

public class Util {
    public static Boolean isNetworkAvailable(Context activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isNetworkAvailable = true;
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        isNetworkAvailable = (activeNetworkInfo != null && activeNetworkInfo
                .isConnectedOrConnecting());
        return isNetworkAvailable;
    }
}
