package com.vivace.parse_application.parseapplication.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.vivace.parse_application.parseapplication.R;

import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button buttonAdd;
    private Button buttonSyncOffline;
    private CheckBox checkBoxOffline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAdd = findViewById(R.id.buttonAdd);
        buttonSyncOffline = findViewById(R.id.buttonSyncOffline);
        checkBoxOffline = findViewById(R.id.checkBoxOffline);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createParseObject(checkBoxOffline.isChecked());
            }
        });

        buttonSyncOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("GameScore");
                        query.fromPin("MyChanges");
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> scores, ParseException e) {
                        for (ParseObject score : scores) {
                            score.saveInBackground();
                            score.unpinInBackground("MyChanges");
                        }
                    }
                });
            }
        });
    }

    private void createParseObject(Boolean offline) {
        Random random = new Random();

        ParseObject gameScore = new ParseObject("GameScore");
        gameScore.put("score", random.nextInt(500));
        gameScore.put("playerName", "Sean Plott");
        gameScore.put("cheatMode", false);

        if (offline) {
            gameScore.pinInBackground("MyChanges");
        } else {
            gameScore.saveEventually(); // it will be pinned until it can be saved
//            gameScore.saveInBackground(); // it will save only if is online
        }
    }
}
